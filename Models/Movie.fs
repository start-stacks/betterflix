namespace betterflix.Models

open System
open System.ComponentModel.DataAnnotations

type Movie() = 
    [<Key>] member val Id = Guid.NewGuid() with get, set
    [<Required>] member val Title = "" with get, set
    member val Director = "" with get, set
    member val ReleaseYear = 0 with get, set