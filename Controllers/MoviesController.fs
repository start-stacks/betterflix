namespace betterflix.Controllers

open System
open System.Collections.Generic
open System.Linq
open System.Threading.Tasks
open Microsoft.AspNetCore.Mvc
open betterflix.Models

[<Route("api/[controller]")>]
type MoviesController () = 
    inherit Controller ()

    [<HttpGet>]
    member this.Get() = 
        seq {yield Movie(Title="Jaws", Director="Steven Spielberg", ReleaseYear=1975)
             yield Movie(Director="George Lucas", ReleaseYear=1977)
             yield Movie(Title="Batman Begins", Director="Christopher Nolan")}